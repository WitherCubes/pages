# withercubes.codeberg.page
This is the source code of my personal website.

## Portions of this repository are licensed as follows:
The source code of this website is licensed under the [MIT License](LICENSE).

Blog posts are licensed under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0) unless otherwise noted.
